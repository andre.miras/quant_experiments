"""Provides trading data sources."""
import datetime
import os
from urllib import request

import pandas as pd


def timestamp_to_datetime(timestamp):
    return datetime.datetime.fromtimestamp(float(timestamp))


def download_from_bitcoincharts(csv_file: str):
    url = f"https://api.bitcoincharts.com/v1/csv/{csv_file}"
    print(f"Downloading from {url} ...")
    request.urlretrieve(url)


def download_from_cryptodatadownload(csv_file: str):
    """http://www.cryptodatadownload.com/data/northamerican/"""
    url = f"http://www.cryptodatadownload.com/cdd/{csv_file}"
    print(f"Downloading from {url} ...")
    request.urlretrieve(url, filename=csv_file)


def get_bitcoincharts_data_frame(csv_file: str):
    """Data frame from bitcoincharts.com require some post processing."""
    if not os.path.isfile(csv_file):
        print(f"File {csv_file} not found.")
        download_from_bitcoincharts(csv_file)
    data_frame = pd.read_csv(
        csv_file,
        names=("Datetime", "Price", "Volume"),
        index_col=0,
        parse_dates=True,
        date_parser=timestamp_to_datetime,
    )
    data_frame = data_frame["Price"].resample("D").ohlc()
    data_frame.columns = data_frame.columns.str.capitalize()
    return data_frame


def get_data_frame(csv_file: str):
    if not os.path.isfile(csv_file):
        print(f"File {csv_file} not found.")
        download_from_cryptodatadownload(csv_file)
    data_frame = pd.read_csv(
        csv_file,
        # don't infer header, but remaps names as expected by backtesting.py
        header=0,
        names=(
            "Date",
            "Symbol",
            "Open",
            "High",
            "Low",
            "Close",
            "Volume ETH",
            "Volume",
        ),
        # skips the banner row
        skiprows=1,
        index_col=0,
        parse_dates=True,
        date_parser=lambda date: datetime.datetime.strptime(date, "%Y-%m-%d"),
    ).sort_index()
    return data_frame
