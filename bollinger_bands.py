"""Yet another Bollinger Bands by low sell high strategy."""
import datetime

import pandas as pd
from backtesting import Backtest, Strategy
from ta.volatility import bollinger_hband, bollinger_lband, bollinger_mavg

from data_source import get_data_frame


def is_oversold(prices: pd.Series, bollinger_band_low: pd.Series):
    return prices[-1] < bollinger_band_low[-1]


def is_overbought(prices: pd.Series, bollinger_band_high: pd.Series):
    return prices[-1] > bollinger_band_high[-1]


class BollingerBandsCross(Strategy):
    # number of periods (e.g. days) to keep the position
    periods_open = 3
    std_mul = 2
    window = 20

    def init(self):
        self._periods_open = self.periods_open
        # TODO: maybe we want to configure on Close, Open, Max here
        close_prices = pd.Series(self.data.Close)
        self.rolling_mean = self.I(
            bollinger_mavg, close_prices, n=self.window, overlay=True,
        )
        self.bollinger_hband = self.I(
            bollinger_hband,
            close_prices,
            n=self.window,
            ndev=self.std_mul,
            overlay=True,
        )
        self.bollinger_lband = self.I(
            bollinger_lband,
            close_prices,
            n=self.window,
            ndev=self.std_mul,
            overlay=True,
        )

    def next(self):
        if is_oversold(self.data.Close, self.bollinger_lband):
            self.buy()
        elif is_overbought(self.data.Close, self.bollinger_hband):
            self.sell()
        else:
            self._periods_open -= 1
            if self._periods_open <= 0:
                self.position.close()
                self._periods_open = self.periods_open


def main():
    date_from = datetime.date(year=2019, month=6, day=1)
    date_to = datetime.date(year=2019, month=11, day=1)
    data = get_data_frame("Coinbase_ETHUSD_d.csv")
    data = data[date_from:date_to]
    bt = Backtest(data, BollingerBandsCross, cash=10000, commission=0.002)
    # tweaks parameters, e.g. `window` from here:
    results = bt.optimize(window=(20, 30), std_mul=(1.5, 2))
    # results = bt.run()
    print(results)
    bt.plot(results=results)


if __name__ == "__main__":
    main()
