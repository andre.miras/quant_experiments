# Quant experiments

This is an experimental repository on cryptocurrency technical analysis for fun an profit.

## Bollinger bands
See explanation and demo in [`bollinger_bands.ipynb`](bollinger_bands.ipynb).
<https://nbviewer.jupyter.org/urls/gitlab.com/andre.miras/quant_experiments/raw/develop/bollinger_bands.ipynb>

See implementation in [`bollinger_bands.py`](bollinger_bands.py).

To give it a try yourself:
```sh
make run
```

## Troubleshooting
Signals open rather the browser.
https://github.com/signalapp/Signal-Desktop/issues/3602
```sh
xdg-mime query default text/html
```
Fix:
```sh
xdg-mime default brave.desktop text/html
```
